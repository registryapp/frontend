import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import Register from './register';
import Login from './login';
import userInfo from './userinfo';
import myRegister from './myRegister';
import addProcess from './addProcess';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={App} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/userinfo" component={userInfo} />
            <Route path="/myregister" component={myRegister} />
            <Route path="/process" component={addProcess} />
        </div>
    </Router>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
