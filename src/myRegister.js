import React from 'react';
import { Redirect } from 'react-router-dom';

export default class myRegister extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userinfo: false,
            process: false,
            username: this.props.location.state.username,
            csrf_token: this.props.location.state.csrf_token,
            logout_token: this.props.location.state.logout_token,
            token_type: this.props.location.state.token_type,
            access_token: this.props.location.state.access_token,
            refresh_token: this.props.location.state.refresh_token,
            processdata: [],
        }
    }

    toUserInfo = event => {
        event.preventDefault();

        this.setState({
            userinfo: true,
        })
    }

    addProcess = event => {
        event.preventDefault();

        this.setState({
            process: true,
        })
    }

    getProcesses = event => {
        event.preventDefault();

        fetch('http://registryapp.lndo.site:8000/rest/views/verwerkingen?_format=json', {
            method: 'GET',
            headers: {
                'Authorization': this.state.token_type + ' ' + this.state.access_token,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-Token': this.state.csrf_token,
                'Access-Control-Allow-Origin': 'http://localhost:3000'
            },
        }).then(response => {
            if(response.ok) {
                console.log(response.json());
                return response.json();
            }

            throw new Error('Something went wrong');
        }).then(data => {
            console.log(data);
        }).catch(function(error) {
            console.log('There was a problem: ', error.message);
        })
    }

    render() {
        const { userinfo } = this.state;
        const { process } = this.state;
        const { processdata } = this.state;

        const {csrf_token} = this.state;
        console.log({csrf_token});

        const {logout_token} = this.state;
        console.log({logout_token});

        const {access_token} = this.state;
        console.log({access_token});

        if(userinfo) {
            return <Redirect to={{
                pathname: '/userinfo',
                state: {
                    username: this.state.username,
                    csrf_token: this.state.csrf_token,
                    logout_token: this.state.logout_token,
                    access_token: this.state.access_token,
                    token_type: this.state.token_type,
                    refresh_token: this.state.refresh_token
                }
            }}/>;
        }

        if(process) {
            return <Redirect to={{
                pathname: '/process',
                state : {
                    username: this.state.username,
                    csrf_token: this.state.csrf_token,
                    logout_token: this.state.logout_token,
                    access_token: this.state.access_token,
                    token_type: this.state.token_type,
                    refresh_token: this.state.refresh_token
                }
            }}/>;
        }

        return (
            <React.Fragment>
                <h1>Mijn verwerkingsregister</h1>
                <br />
                <button onClick={this.toUserInfo}>Mijn account</button>
                <br />
                <br />
                <button onClick={this.addProcess}>Verwerking toevoegen</button>
                <br />
                <br />
                <button onClick={this.getProcesses}>Mijn verwerkingen</button>
            </React.Fragment>
        )
    }
}