import React from 'react'
import { Redirect } from 'react-router-dom';
export default class register extends React.Component {
  state = {
    username: "",
    mail: "",
    password: "",
    toLogin: false,
  }

  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value,
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    fetch('http://registryapp.lndo.site:8000/user/register?_format=json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:3000'
      },
      body: JSON.stringify({
        name: { value: this.state.username },
        mail: { value: this.state.mail },
        pass: { value: this.state.password }
      })
    })
    .then(function(response) {
      if(response.ok) {
        return response.json()
      }
      throw new Error('Something went wrong')
    })
    .then(data => this.setState({ 
      toLogin: true 
    }))
    .catch(function(error) {
      console.log('There was a problem: ', error.message)
    })
  }

  render() {
    const {toLogin} = this.state;

    if(toLogin) {
      
      return <Redirect to={{
        pathname: '/login',
      }}/>;
  }

    return (
        <form onSubmit={this.handleSubmit}>
          Gebruikersnaam:
          <br />
          <input type="text" name="username" value={this.state.username} onChange={this.handleInputChange} />
          <br />
          <br />
          Wachtwoord:
          <br />
          <input type="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
          <br />
          <br />
          E-mail:
          <br />
          <input type="text" name="mail" value={this.state.mail} onChange={this.handleInputChange} />
          <br />
          <br />
          <button type="submit">Aanmelden</button>
        </form>
    )
  }
}