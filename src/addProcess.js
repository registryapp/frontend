import React from 'react';
import { Redirect } from 'react-router-dom';

export default class addProcess extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            myRegister: false,
            username: this.props.location.state.username,
            csrf_token: this.props.location.state.csrf_token,
            logout_token: this.props.location.state.logout_token,
            token_type: this.props.location.state.token_type,
            access_token: this.props.location.state.access_token,
            refresh_token: this.props.location.state.refresh_token,
            betrokkenen: "",
            doeleinden: "",
            ontvangers: "",
            persoonsgegevens: "",
            verwerkersovereenkomst: "",
            wettelijke_grondslag: "",
            afspraken_datalekken: "",
            landen: "",
            bewaartermijn: "",
            passende_waarborgen: "",
            pia_uitgevoerd: "",
            veiligheidsmaatregelen: "",
            title: "",
        }
    }

    handleInputChange = event => {
        const target = event.target
        const value = target.value
        const name = target.name
    
        this.setState({
          [name]: value,
        })
    }

    handleSubmit = event => {
        event.preventDefault();

        fetch('http://registryapp.lndo.site:8000/node?_format=json', {
            method: 'POST',
            headers: {
                'Authorization': this.state.token_type + ' ' + this.state.access_token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRF-Token': this.state.csrf_token
            },
            body: JSON.stringify({
                type: [{ target_id: "verwerking" }],
                title: [{ value: this.state.title }],
                field_betrokkenen: [{ value: this.state.betrokkenen }],
                field_doeleinden: [{ value: this.state.doeleinden }],
                field_ontvangers: [{ value: this.state.ontvangers }],
                field_persoonsgegevens: [{ value: this.state.persoonsgegevens }],
                field_verwerkersovereenkomst: [{ value: this.state.verwerkersovereenkomst }],
                field_wettelijke_grondslag: [{ value: this.state.wettelijke_grondslag }],
                field_afspraken_datalekken: [{ value: this.state.afspraken_datalekken }],
                field_landen: [{ value: this.state.landen }],
                field_passende_waarborgen: [{ value: this.state.passende_waarborgen }],
                field_pia_uitgevoerd: [{ value: this.state.pia_uitgevoerd }],
                field_bewaartermijn: [{ value: this.state.bewaartermijn }],
                field_veiligheidsmaatregelen: [{ value: this.state.veiligheidsmaatregelen }]
            })
        }).then(function(response) {
            if(response.ok) {
                alert('De verwerking is toegevoegd!');
                return response.json();
            }

            throw new Error('Something went wrong');
        }).catch(function(error) {
            console.log('There was a problem: ', error.message);
        })
    }

    toMyRegister = event => {
        event.preventDefault();

        this.setState({
            myRegister: true,
        })
    }

    render() {
        const { myRegister } = this.state;

        if(myRegister) {
            return <Redirect to={{
                pathname: '/myregister',
                state: {
                    username: this.state.username,
                    csrf_token: this.state.csrf_token,
                    logout_token: this.state.logout_token,
                    access_token: this.state.access_token,
                    token_type: this.state.token_type,
                    refresh_token: this.state.refresh_token
                }
            }}/>;
        }

        return (
            <React.Fragment>
                <h1>Verwerking toevoegen</h1>
                <br />
                <button onClick={this.toMyRegister}>Terug</button>

                <br />
                <br />

                <form onSubmit={this.handleSubmit}>
                    Titel:
                    <br />
                    <textarea type="text" name="title" value={this.state.title} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Betrokkenen:
                    <br />
                    <textarea type="text" name="betrokkenen" value={this.state.betrokkenen} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Persoonsgegevens: 
                    <br />
                    <textarea type="text" name="persoonsgegevens" value={this.state.persoonsgegevens} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Doeleinden:
                    <br />
                    <textarea type="text" name="doeleinden" value={this.state.doeleinden} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Ontvangers:
                    <br />
                    <textarea type="text" name="ontvangers" value={this.state.ontvangers} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Wettelijke grondslag:
                    <br />
                    <textarea type="text" name="wettelijke_grondslag" value={this.state.wettelijke_grondslag} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Verwerkersovereenkomst: 
                    <br />
                    <textarea type="text" name="verwerkersovereenkomst" value={this.state.verwerkersovereenkomst} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Afspraken datalekken:
                    <br />
                    <textarea type="text" name="afspraken_datalekken" value={this.state.afspraken_datalekken} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    PIA uitgevoerd:
                    <br />
                    <textarea type="text" name="pia_uitgevoerd" value={this.state.pia_uitgevoerd} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Landen:
                    <br />
                    <textarea type="text" name="landen" value={this.state.landen} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Passende waarborgen artikel 49 AVG:
                    <br />
                    <textarea type="text" name="passende_waarborgen" value={this.state.passende_waarborgen} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Bewaartermijn:
                    <br />
                    <textarea type="text" name="bewaartermijn" value={this.state.bewaartermijn} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    Veiligheidsmaatregelen:
                    <br />
                    <textarea type="text" name="veiligheidsmaatregelen" value={this.state.veiligheidsmaatregelen} onChange={this.handleInputChange} />
                    <br />
                    <br />
                    <button type="submit">Verwerking toevoegen</button>
                </form>
            </React.Fragment>
        )
    }
}