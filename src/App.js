import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Creeër overzicht met RegistryApp</h1>
        <p className="intro-txt">Gemakkelijk, snel en volgens de eisen van de AVG een verwerkingsregister bijhouden</p>
        <Link to='/register'>Begin nu</Link>
      </div>
    );
  }
}

export default App;
