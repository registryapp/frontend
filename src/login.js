import React from 'react';
import { Redirect } from 'react-router-dom';
import qs from 'qs';
export default class login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      toUserinfo: false,
      client_secret: "1234",
      grant_type: "password",
      client_id: "a8a14d67-5df8-4241-8066-038ce94f05cd",
      scope: "privacy_manager"
    }
  }

  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value,
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    fetch('http://registryapp.lndo.site:8000/user/login?_format=json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:3000'
      },
      body: JSON.stringify({
        name: this.state.username,
        pass: this.state.password
      })
    })
        .then(function(response) {
          if(response.ok) {
            return response.json()
          }
          throw new Error('Something went wrong')
        })
        .then(data => {
          this.setState({
            csrf_token: data.csrf_token,
            logout_token: data.logout_token
          })

          return fetch('http://registryapp.lndo.site:8000/oauth/token', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Access-Control-Allow-Origin': 'http://localhost:3000'
            },
            body: qs.stringify({
              username: this.state.username,
              password: this.state.password,
              client_secret: this.state.client_secret,
              grant_type: this.state.grant_type,
              client_id: this.state.client_id,
              scope: this.state.scope
            })
          })
        })
        .then(function(response) {
          if(response.ok) {
            return response.json()
          }
          throw new Error('Authorization went wrong')
        })
        .then(data => this.setState({ 
          token_type: data.token_type,
          access_token: data.access_token,
          refresh_token: data.refresh_token,
          toUserinfo: true 
        }))
        .catch(function(error) {
          console.log('There was a problem: ', error.message)
        })
  }

  render() {
    const {toUserinfo} = this.state;

    if(toUserinfo) {
      
        return <Redirect to={{
          pathname: '/userinfo',
          state: {
            username: this.state.username,
            csrf_token: this.state.csrf_token,
            logout_token: this.state.logout_token,
            access_token: this.state.access_token,
            token_type: this.state.token_type,
            refresh_token: this.state.refresh_token
          },
        }}/>;
    }

    return (
      <form onSubmit={this.handleSubmit}>
          Gebruikersnaam:
          <br />
          <input type="text" name="username" value={this.state.username} onChange={this.handleInputChange} />
          <br />
          <br />
          Wachtwoord:
          <br />
          <input type="password" name="password" value={this.state.password} onChange={this.handleInputChange} />
          <br />
          <br />
          <button type="submit">Login</button>
        </form>
    )
  }
}