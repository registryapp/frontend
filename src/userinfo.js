import React from 'react';
import { Redirect } from 'react-router-dom';

export default class userinfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: this.props.location.state.username,
            csrf_token: this.props.location.state.csrf_token,
            logout_token: this.props.location.state.logout_token,
            token_type: this.props.location.state.token_type,
            access_token: this.props.location.state.access_token,
            refresh_token: this.props.location.state.refresh_token,
            toHome: false,
            toRegister: false,
        }
    }

    logoutUser = event => {
        event.preventDefault();

        fetch('http://registryapp.lndo.site:8000/user/logout?token=' + this.state.logout_token, {
            method: 'POST',
            headers: {
                'Authorization': this.state.token_type + ' ' + this.state.access_token,
                'Content-Type': 'x-www-form-urlencoded',
                'X-CSRF-Token': this.state.csrf_token,
                'Access-Control-Allow-Origin': 'http://localhost:3000'
            },
        }).then(response => {
            if(response.ok) {
                return response;
            }
            throw new Error('Something went wrong')
        }).then(data => this.setState({
            toHome: true,
        }))
        .catch(function(error) {
            console.log('There was a problem: ', error.message)
          })
    }

    toMyRegister = event => {
        event.preventDefault();

        this.setState({
            toRegister: true,
        })
    }

    render() {
        const { toHome } = this.state;
        const { toRegister } = this.state;

        if(toHome) {
            return <Redirect to={{
                pathname: '/',
            }}/>;
        }

        if(toRegister) {
            return <Redirect to={{
                pathname: '/myregister',
                state: {
                    username: this.state.username,
                    csrf_token: this.state.csrf_token,
                    logout_token: this.state.logout_token,
                    access_token: this.state.access_token,
                    token_type: this.state.token_type,
                    refresh_token: this.state.refresh_token
                }
            }}/>;
        }

        const {csrf_token} = this.state;
        console.log({csrf_token});

        const {logout_token} = this.state;
        console.log({logout_token});

        const {access_token} = this.state;
        console.log({access_token});

        return (
            <React.Fragment>
                <h1>Welkom {this.state.username}</h1>
                <br />
                <button onClick={this.logoutUser}>Uitloggen</button>
                <br />
                <br />
                <button onClick={this.toMyRegister}>Mijn verwerkingsregister</button>
            </React.Fragment>
        )
    }
}